//
//  SampleAppUITests.swift
//  SampleAppUITests
//
//  Created by Roman on 30/06/2020.
//  Copyright © 2020 Rabobank. All rights reserved.
//

import XCTest
import SampleApp

class SampleAppUITests: XCTestCase {

    func testPassing() throws {
        let app = XCUIApplication()
        app.launch()

        app.buttons["Button"].tap()
        
        let field = app.textFields["TextField"]
        XCTAssert(field.value as! String == "ToggleOn")

        let fullScreenshot = XCUIScreen.main.screenshot()
        let screenshot = XCTAttachment(screenshot: fullScreenshot)
        screenshot.lifetime = .keepAlways
        add(screenshot)
    }
    
    func testFailing() throws {
        let app = XCUIApplication()
        app.launch()

        let field = app.textFields["TextField"]
        XCTAssert(field.value as! String == "asdasdasdasds")
    }
    
    func testFlaky() throws {
        let app = XCUIApplication()
        app.launch()

        for _ in 1...Int.random(in: 2..<11) {
            app.buttons["Button"].tap()
        }
        
        
        let field = app.textFields["TextField"]
        XCTAssert(field.value as! String == "ToggleOn")
    }
    
   
}
